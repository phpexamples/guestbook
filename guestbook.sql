drop database if exists guestbook;

create database guestbook;

use guestbook;

create table message (
    id int primary key auto_increment,
    name varchar(100) not null,
    message text,
    saved timestamp default current_timestamp
);
