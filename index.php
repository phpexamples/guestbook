<?php
$search = '';
$criteria='';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $search = filter_input(INPUT_POST,'search',FILTER_SANITIZE_STRING);
    if (strlen($search) > 0) {
        $criteria = "where name like '%$search%' or message like '%$search%' ";
    }
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="index.php">Guestbook</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
              <ul class="navbar-nav">
                <li class="nav-item">
                  <a class="nav-link" href="add.php">Add message</a>
                </li>
            </div>
        </nav>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h3>View guestbook</h3>
                    <div id="search">
                        <form class="form-inline" action="<?php print $_SERVER['PHP_SELF']; ?>" method="post">
                            <div class="form-group">
                                <input value="<?php print $search;?>" name="search" class="form-control" maxlength="20" placeholder="Type search criteria...">
                                <button type="submit" class="btn btn-primary">Search</button>
                            </div>
                        </form>
                    </div>
                    <?php
                    try {
                        $db = new PDO('mysql:host=localhost;dbname=guestbook;charset=utf8','root','root1234');
                        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                        
                        $sql = "select * from message ";
                        
                        if (strlen($criteria) > 0) {
                            $sql .= $criteria;
                        }
                        $sql .="order by saved desc";
                        $statement = $db->query($sql);
                        if ($statement) {
                            while ($record = $statement->fetch()) {
                                print "<div class='message'>";
                                print "<p>";
                                print $record['saved'] . " by " . $record['name'] . '<br />';
                                print htmlspecialchars($record['message']);
                                print "</p>";
                                print "<span class='delete'><a href='delete.php?id=" . $record['id'] . "'>Delete</a></span>";
                                print "</div>";
                            }
                        }
                        else {
                            print "<p>Error retrieving messages.</p>";
                        }
                       
                    }
                    catch (Exception $ex) {
                        print "<p>Failure in database connection. " . $ex->getMessage() . "</p>";
                    }
                    ?>
                </div>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </body>
</html>
