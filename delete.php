<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="index.php">Guestbook</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
              <ul class="navbar-nav"> 
                <li class="nav-item">
                    <a class="nav-link" href="add.php">Add message</a>
                </li>
            </div>
        </nav>
        <div class="container">
            <div class="row">
                <div class="col-lg-12"> 
                    <?php
                    if ($_SERVER['REQUEST_METHOD'] === 'GET') {
                        try {
                            $db = new PDO('mysql:host=localhost;dbname=guestbook;charset=utf8','root','root1234');
                            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                                
                            $id = filter_input(INPUT_GET,'id',FILTER_SANITIZE_NUMBER_INT);
                                 
                            $sql = "delete from message where id = :id";
                            
                            $statement = $db->prepare($sql);
                            $statement->bindValue(':id', $id,PDO::PARAM_INT);
                             
                            $statement->execute();
                            
                            
                            print "<p>Message deleted</p>";
                            print "<a href='index.php'>Go to messages</a>";
                        }
                        catch (Exception $ex) {
                            print "<p>Failure in database connection. " . $ex->getMessage() . "</p>";
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </body>
</html>
